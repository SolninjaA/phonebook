'''
    Name: Solninja A
    YouTube Channel: Solninja A
    Twitch Channel: iamsolninjaa
    Project Name: Phonebook
'''

import time


def Insert():
    phoneBook = open("myfile.txt", "a")
    toAdd = input("What name & phone number would you like to add?: ")
    phoneBook.write(toAdd + "\n")
    print("Added '" + toAdd + "' to the phonebook!")

    phoneBook.close()
    



def Display():
    phoneBook = open("myFile.txt", "r")

    print("\n Here is your phonebook list:")
    print(phoneBook.read())


def Clear():
    check = input("Are you sure you want to clear ALL of your phonebook! answer with y/n (This is permanent): ")
    if check == "y":
        timer = 10
        for i in range(10): 
            clearing = print("Clearing in " + str(timer) + " seconds.\nIF YOU WANT TO CANCEL PRESS ENTER")
            time.sleep(1)
            timer -= 1

        if clearing == "":
            pass
        phoneBook = open("myFile.txt", "w")
        phoneBook.close()

    else:
        pass


# Intro
print("Hello, welcome to your Phonebook\n")
print("How can I help you?: ")
print(" 1: Add a name & phone number to your phonebook\n 2: Display your phonebook\n 3: Clear your phonebook \n 4: Quit")

while True:
    choice = input("Please select an option: ")
    if choice == "1":
        Insert()

    elif choice == "2":
        Display()

    elif choice == "3":
        Clear()

    elif choice == "4":
        quit()

    else:
        print("Sorry " + choice + " is not an option please try again")
